package ds2.oss.spring.jmx.tests;

import ds2.oss.spring.jmx.MyConfig;
import ds2.oss.spring.jmx.impl.AppConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.lang.invoke.MethodHandles;

/**
 * Created by deindesign on 01.04.15.
 */
@ContextConfiguration(classes = AppConfig.class)
public class DummyRunnerTest extends AbstractTestNGSpringContextTests{
    private static final Logger LOG= LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    @Autowired
    private MyConfig config;

    @Test
    public void testInfinite() throws InterruptedException {
        Assert.assertNotNull(config);
        LOG.info("Waiting now 30 seconds to test jmx ;)");
        for(int i=0;i<30;i++){
            LOG.info("Count is now {}", config.getCount());
            Thread.sleep(1000);
        }
        LOG.info("Done");
    }
}
