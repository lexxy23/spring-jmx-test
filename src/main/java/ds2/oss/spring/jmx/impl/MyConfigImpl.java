package ds2.oss.spring.jmx.impl;

import ds2.oss.spring.jmx.MyConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

/**
 * Created by deindesign on 01.04.15.
 */
@Component
@ManagedResource
public class MyConfigImpl implements MyConfig{
    private static final Logger LOG= LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private int count=10;

    @ManagedAttribute
    @Override public int getCount() {
        return count;
    }
    @ManagedAttribute
    public void setCount(int c){
        count=c;
        LOG.info("New value for count is {}", count);
    }
}
