package ds2.oss.spring.jmx.impl;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;

/**
 * Created by deindesign on 01.04.15.
 */
@Configuration
@ComponentScan(basePackages = "ds2.oss.spring.jmx.impl")
@EnableMBeanExport
public class AppConfig {
}
