# Spring JMX Test #

This sample project should give you a starting point to setup jmx with spring. In it's test run, it will setup the jmx bean and give you the opportunity to access it via jconsole (or any other jmx tool) to change a value. This value is printed on change, and continuously to the std output stream.

Have fun.